define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.DashboardCtrl', [])
    .controller('DashboardCtrl', ['$scope', 'Http', '$routeParams', 'UserService', function ($scope, Http, $routeParams, UserService) {
      $scope.createCommentData = {
        error: ''
      };

      $scope.ads = [];
      $scope.comments = [];

      var user = UserService.getCurrentUser();
      var ownerId = $routeParams.userId;

      var fetchAds = function() {
        Http.makeApiCall({
          method: 'GET',
          url: 'userAds/' + ownerId
        }).then(function(response) {
          var ads = response.data;
          $scope.ads = ads;
        });
      };

      var fetchComments = function() {
        Http.makeApiCall({
          method: 'GET',
          url: 'user/comment/' + ownerId
        }).then(function(response) {
          var comments = response.data;
          $scope.comments = comments;
        });
      };

      fetchAds();
      fetchComments();

      $scope.onSubjectClick = function(ad) {
        $scope.changeView('/user/ad-details/' + ad._id);
      };

      $scope.onCreateAdClick = function() {
        $scope.changeView('/admin/create-ad');
      };

      $scope.onCreateCommentClick = function() {
        Http.makeApiCall({
          method: 'POST',
          url: 'user/comment',
          data: {
            description: $scope.createCommentData.description,
            userId: user._id,
            userName: user.name,
            ownerId: ownerId
          }
        }).then(function(response) {
          var comment = response.data;
          $scope.comments.push(comment);
        }).catch(function(error) {
          if (error.status === 401) {
            $scope.createCommentData.error = "Greška";
            $scope.changeView('/error');
          }
        });
      };

      $scope.onCommentDeleteClick = function(comment) {
        Http.makeApiCall({
          method: 'DELETE',
          url: 'user/comment',
          data: {
            commentId: comment._id
          }
        }).then(function(response) {
          var comment = response.data;
          console.log('obrisano');
        });
      };

      $scope.onProfileClick = function() {
        $scope.changeView('/user/profile/' + ownerId);
      };
  }]);
});