define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.logout.LogoutCtrl', [])
    .controller('LogoutCtrl', ['$scope', 'Http', 'UserService', function ($scope, Http, UserService) {	
    
  		Http.makeApiCall({
        method: 'GET',
        url: 'logout',
      }).then(function(response) {
        UserService.setCurrentUser({});
        $scope.changeView('/login');
      });	
    

	}]);
});



