define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.MainCtrl', [])
    .controller('MainCtrl', ['$scope', function ($scope) {

      $scope.user = {
        "id": 6,
        "userId": 17,
        "content": "Moje preporuke svima"
      };


    }]);
});
