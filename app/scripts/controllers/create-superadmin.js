define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.CreateSuperadminCtrl', [])
    .controller('CreateSuperadminCtrl', ['$scope', 'Http', function ($scope, Http) {
      $scope.onSuperadminCreateClick = function() {
	      Http.makeApiCall({
	        method: 'GET',
	        url: 'createSuperadmin',
	      }).then(function(response) {
	        var superadmin = response.data;
	        console.log(superadmin);
	      });
     	}
  }]);
});
