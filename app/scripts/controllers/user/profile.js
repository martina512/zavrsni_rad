define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.user.ProfileCtrl', [])
    .controller('ProfileCtrl', ['$scope', 'Http', '$routeParams', 'UserService', function ($scope, Http, $routeParams, UserService) {
      var userId = $routeParams.userId;
      var currentUser = UserService.getCurrentUser();

      $scope.isCurrentUser = currentUser._id === userId;

      Http.makeApiCall({
        method: 'GET',
        url: 'user/' + userId,
      }).then(function(response) {
        var profile = response.data;
        $scope.user = profile; 
      });

      $scope.onSaveEditClick = function(user) {
        Http.makeApiCall({
          method: 'PUT',
          url: 'user/' + userId,
          data: $scope.user
        }).then(function(response) {
          var updatedUser = response.data;
          console.log(updatedUser);
        });
      }

    }]);
});
