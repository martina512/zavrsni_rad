define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.user.DisplayingAdsCtrl', [])
    .controller('DisplayingAdsCtrl', ['$scope', '$http', 'Http', function ($scope, $http, Http){

      $scope.filters = {
        category: undefined,
        price: undefined
      }
      
      $scope.filters = {
        category: $scope.filters.category,
        price: $scope.filters.price
      };
      $scope.ads = [];

      $scope.onFilterClick = function() {
        $http({
          method: 'GET',
          url: 'http://localhost:3000/ads',
          params: $scope.filters
        }).then(function(response) {
          var ads = response.data;
          $scope.ads = ads; 
        });
      };

      $scope.onFilterClick();

      $scope.onCreateAdClick = function() {
        $scope.changeView('/admin/create-ad');
      };

      $scope.onDeleteAdClick = function(ad) {
        Http.makeApiCall({
          method: 'DELETE',
          url: 'ad',
          data: {
            adId: ad._id
          }
        }).then(function(response) {
          var ad = response.data;
        });
        console.log('obrisano');
      };

      $scope.onEditAdClick = function(ad) {
        $scope.changeView('/admin/edit-ad/' + ad._id);
      };

      $scope.onSubjectClick = function(ad) {
        $scope.changeView('/user/ad-details/' + ad._id);
      };
    }]);
});
