define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.user.AdDetailsCtrl', [])
    .controller('AdDetailsCtrl', ['$scope', 'Http', '$routeParams', 'UserService', function ($scope, Http, $routeParams, UserService) {
      $scope.createCommentData = {
        error: ''
      };

      var adId = $routeParams.adId;
      var currentUser = UserService.getCurrentUser();

      Http.makeApiCall({
        method: 'GET',
        url: 'ad/' + adId,
      }).then(function(response) {
        $scope.ad = response.data;
      });

      $scope.onNameClick = function(ad) {
        $scope.changeView('/dashboard/' + ad.userId);
      };

      $scope.onCreateCommentClick = function() {
        Http.makeApiCall({
          method: 'POST',
          url: 'ad/comment',
          data: {
            description: $scope.createCommentData.description,
            adId: adId,
            userName: currentUser.name
          }
        }).then(function(response) {
          var comment = response.data;
          $scope.comments.push(comment);
          console.log(comment); 
          $scope.changeView('/user/ad-details/' + adId);
        }).catch(function(error) {
          if (error.status === 401) {
            $scope.createCommentData.error = "Greška"
            $scope.changeView('/error');
          }
        });

      }

      $scope.comments = [];

      Http.makeApiCall({
        method: 'GET',
        url: 'ad/comment/' + adId,
      }).then(function(response) {
        var comments = response.data;
        $scope.comments = comments; 
      });

      $scope.onCommentDeleteClick = function(comment) {
        Http.makeApiCall({
          method: 'DELETE',
          url: 'ad/comment',
          data: {
            commentId: comment._id
          }
        }).then(function(response) {
          var comment = response.data;
        });
        console.log('obrisano');
      };

    }]);
});
