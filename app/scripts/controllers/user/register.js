define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.user.RegisterCtrl', [])
    .controller('RegisterCtrl', ['$scope', '$http', function ($scope, $http) {
      $scope.registerData = {
        error: ''
      };

      $scope.onRegisterClick = function() {
        $http({
        method: 'POST',
        url: 'http://localhost:3000/register',
        data: {
          name: $scope.registerData.name,
          surname: $scope.registerData.surname,
          email: $scope.registerData.email,
          password: $scope.registerData.password,
          education: $scope.registerData.education,
          city: $scope.registerData.city,
          postNumber: $scope.registerData.postNumber,
          address: $scope.registerData.address,
          county: $scope.registerData.county
        }
        }).then(function(response) {
          var reg = response.data;
          console.log(reg);
          $scope.registerData.register = reg; 
          $scope.changeView('/dashboard');
        }).catch(function(error) {
          if (error.status === 401) {
            $scope.registerData.error = "Neuspješna registracija, korisnik s tom email adresom je već registriran!"
        
          }
        });

      }
      
    }]);
});
