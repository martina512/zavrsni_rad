define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.user.LoginCtrl', [])
    .controller('LoginCtrl', ['$scope', 'Http', 'UserService', function ($scope, Http, UserService) {
    	$scope.loginData = {
        error: ''
      };
      
      $scope.onLoginClick = function() {
        Http.makeApiCall({
          method: 'POST',
          url: 'login',
          data: {
            email: $scope.loginData.email,
            password: $scope.loginData.password
          }
        }).then(function(response) {
          var user = response.data;
          UserService.setCurrentUser(user);
          $scope.changeView('/user/profile/' + user._id);
        }).catch(function(error) {
          if (error.status === 401) {
            $scope.loginData.error = "Email adresa ili lozinka nisu ispravni !";
          }
        });
      }

    }]);
});