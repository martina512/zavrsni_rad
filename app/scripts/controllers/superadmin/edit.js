define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.superadmin.EditCtrl', [])
    .controller('EditCtrl', ['$scope', function ($scope) {
    	$scope.editData = {};

    	$scope.onSaveAdminClick = function () {
    		console.log($scope.editData);
    	};
   
    }]);
});