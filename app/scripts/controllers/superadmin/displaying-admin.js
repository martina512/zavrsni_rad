define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.superadmin.DisplayingAdminCtrl', [])
    .controller('DisplayingAdminCtrl', ['$scope', '$routeParams', 'UserService', 'Http', function ($scope, $routeParams, UserService, Http) {

      $scope.admins = UserService.getCurrentUser();

      var userId = $routeParams.userId;

      Http.makeApiCall({
        method: 'GET',
        url: 'admin',
      }).then(function(response) {
        var admins = response.data;
        $scope.admins = admins; 
      });

      $scope.onDeleteAdminClick = function(admin) {
        Http.makeApiCall({
          method: 'DELETE',
          url: 'user',
          data: {
            userId: admin._id
          }
        }).then(function(response) {
          var admin = response.data;
        });
      };

      $scope.onCreateAdminClick = function() {
        $scope.changeView('/superadmin/add-admin');
      };

      $scope.onEditAdminClick = function(admin) {
        $scope.changeView('/user/profile/' + admin._id);
      }
    }]);
});
