define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.superadmin.AddAdminCtrl', [])
    .controller('AddAdminCtrl', ['$scope', 'Http', function ($scope, Http) {
      $scope.adminData = {};

      $scope.onAddClick = function() {
        var addAdminData = {
          name: $scope.adminData.name,
          surname: $scope.adminData.surname,
          email: $scope.adminData.email,
          password: $scope.adminData.password,
          education: $scope.adminData.education,
          city: $scope.adminData.city,
          postNumber: $scope.adminData.postNumber,
          address: $scope.adminData.address,
          county: $scope.adminData.county
        }
        Http.makeApiCall({
          method: 'POST',
          url: 'admin',
          data: addAdminData
        }).then(function(response) {
          var admin = response.data;
          console.log(admin);
          $scope.changeView('/superadmin/displaying-admin');
        }).catch(function(error) {
          if (error.status === 401) {
            $scope.loginData.error = "Greška";
            $scope.changeView('/error');
          }
        });
      }

    }]);
  });

     // $scope.onAddClick = function() {
       // console.log($scope.adminData);
        //console.log('dodali smo novog admina u ostale admine');
       
        //AdminService.setAdmin($scope.adminData);
        //$scope.changeView('/superadmin');

