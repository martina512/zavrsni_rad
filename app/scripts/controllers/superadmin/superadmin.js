define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.superadmin.SuperadminCtrl', [])
    .controller('SuperadminCtrl', ['$scope', 'AdminService', function ($scope, AdminService) {
    	$scope.onEditClick = function() {
    		console.log('Edit forma');
    		$scope.changeView('/superadmin/edit');
    	}
    	$scope.onAddAdminClick = function() {
    		console.log ('klik na admina');
    		$scope.changeView('/superadmin/new');
    	};

			$scope.adminsData = AdminService.getAdmins();
		}]);
});