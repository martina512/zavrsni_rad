define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.admin.CreateCtrl', [])
    .controller('CreateCtrl', ['$scope', 'Http', function ($scope, Http) {
      $scope.createData = {
        error: ''
      };

      $scope.onCreateClick = function() {
        var userData = {
          name: $scope.createData.name,
          surname: $scope.createData.surname,
          email: $scope.createData.email,
          password: $scope.createData.password,
          education: $scope.createData.education,
          city: $scope.createData.city,
          postNumber: $scope.createData.postNumber,
          address: $scope.createData.address,
          county: $scope.createData.county
        };

        Http.makeApiCall({
          method: 'POST',
          url: 'user',
          data: userData
        }).then(function(response) {
          var user = response.data;
          $scope.createData.create = user; 
          $scope.changeView('/admin/displaying-users');
        }).catch(function() {
          $scope.createData.error = "Greška";
        });
      }
      
    }]);
});
