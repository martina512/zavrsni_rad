define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.admin.DisplayingUsersCtrl', [])
    .controller('DisplayingUsersCtrl', ['$scope', 'Http', function ($scope, Http) {

      $scope.users = [];

      Http.makeApiCall({
        method: 'GET',
        url: 'users',
      }).then(function(response) {
        var users = response.data;
        $scope.users = users; 
      });

      $scope.onDeleteClick = function(user) {
        Http.makeApiCall({
          method: 'DELETE',
          url: 'user',
          data: {
            userId: user._id
          }
        }).then(function(response) {
          var user = response.data;

        });
        console.log('obrisano');
      };

      $scope.onCreateClick = function() {
        $scope.changeView('/admin/create-user');
      };

      $scope.onEditClick = function(user) {
        $scope.changeView('/user/profile/' + user._id);
      };
    }]);
});
