define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.admin.AddUserCtrl', [])
    .controller('AddUserCtrl', ['$scope', function ($scope) {
      $scope.userData = {};

      $scope.onAddUserClick = function() {
        console.log($scope.userData);
      }
    }]);
});