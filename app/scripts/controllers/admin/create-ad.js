define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.admin.CreateAdCtrl', [])
    .controller('CreateAdCtrl', ['$scope', 'Http', function ($scope, Http) {
      $scope.createAdData = {
        error: ''
      };

      $scope.onCreateClick = function() {
        var adData = {
          subject: $scope.createAdData.subject,
          looking: $scope.createAdData.looking,
          offer: $scope.createAdData.offer,
          category: $scope.createAdData.category,
          price: $scope.createAdData.price,
          place: $scope.createAdData.place,
          date: $scope.createAdData.date,
          time: $scope.createAdData.time,
          description: $scope.createAdData.description
        }

        Http.makeApiCall({
          method: 'POST',
          url: 'ad',
          data: adData
        }).then(function(response) {
          var ad = response.data;
          console.log(ad);
          $scope.createAdData.create = ad; 
          $scope.changeView('/user/displaying-ads');
        }).catch(function(error) {
          if (error.status === 401) {
            $scope.createAdData.error = "Greška"
            $location.path('/error');
          }
        });

      }
      
    }]);
});
