define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.admin.AdminCtrl', [])
    .controller('AdminCtrl', ['$scope', function ($scope) {
    	$scope.onEditClick = function() {
    		console.log('Edit forma');
    		$scope.changeView('/admin/edit');
    	};
    	$scope.onAddUserClick = function() {
    		console.log ('dodaj korisnika');
    		$scope.changeView('/admin/new');
    	};
		}]);
});