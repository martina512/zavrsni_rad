define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.controllers.admin.EditAdCtrl', [])
    .controller('EditAdCtrl', ['$scope', 'Http', '$routeParams', 'UserService', function ($scope, Http, $routeParams, UserService) {
      var adId = $routeParams.adId;

      Http.makeApiCall({
        method: 'GET',
        url: 'ad/' + adId,
      }).then(function(response) {
        var editAd = response.data;
        $scope.ad = editAd; 
      });

      $scope.onSaveEditClick = function(ad) {
        Http.makeApiCall({
          method: 'PUT',
          url: 'ad/' + adId,
          data: $scope.ad
        }).then(function(response) {
          var updatedAd = response.data;
          console.log(updatedAd);
          $scope.changeView('/user/displaying-ads');
        });
      }

    }]);
});
