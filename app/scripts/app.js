/*jshint unused: vars */
define([
  'angular', 
  'controllers/main', 
  'controllers/dashboard',
  'controllers/user/register',
  'controllers/user/login',
  'controllers/superadmin/superadmin',
  'controllers/superadmin/edit',
  'controllers/superadmin/add-admin',
  'controllers/admin/admin',
  'controllers/admin/add-user', 
  'controllers/logout/logout',
  'controllers/superadmin/displaying-admin',
  'controllers/user/error', 
  'controllers/admin/displaying-users',
  'controllers/admin/create-user',
  'controllers/user/profile',
  'controllers/admin/create-ad',
  'controllers/user/displaying-ads',
  'controllers/admin/edit-ad',
  'controllers/user/ad-details',
  'controllers/about-us',
  'controllers/create-superadmin',

  'services/userservice',
  'services/adminservice',
  'services/http'
  ], 
  function (
    angular, 
    MainCtrl, 
    DashboardCtrl, 
    RegisterCtrl, 
    LoginCtrl, 
    SuperadminCtrl, 
    EditCtrl, 
    AddAdminCtrl, 
    AdminCtrl, 
    AddUserCtrl, 
    LogoutCtrl,
    DisplayingAdminCtrl,
    ErrorCtrl,
    DisplayingUsersCtrl,
    CreateCtrl,
    ProfileCtrl,
    CreateAdCtrl,
    DisplayingAdsCtrl,
    EditAdCtrl,
    AdDetailsCtrl,
    AboutUsCtrl,
    CreateSuperadminCtrl,
    UserServiceService,
    AdminServiceService,
    HttpService
  ) {
    'use strict';

    return angular
      .module('instrukcijeApp', [
        'instrukcijeApp.controllers.MainCtrl',
        'instrukcijeApp.controllers.DashboardCtrl',
        'instrukcijeApp.controllers.user.RegisterCtrl',
        'instrukcijeApp.controllers.user.LoginCtrl',
        'instrukcijeApp.controllers.superadmin.SuperadminCtrl',
        'instrukcijeApp.controllers.superadmin.EditCtrl',
        'instrukcijeApp.controllers.superadmin.AddAdminCtrl',
        'instrukcijeApp.controllers.admin.AdminCtrl',
        'instrukcijeApp.controllers.admin.AddUserCtrl',
        'instrukcijeApp.controllers.logout.LogoutCtrl',
        'instrukcijeApp.controllers.superadmin.DisplayingAdminCtrl',
        'instrukcijeApp.controllers.user.ErrorCtrl',
        'instrukcijeApp.controllers.admin.DisplayingUsersCtrl',
        'instrukcijeApp.controllers.admin.CreateCtrl',
        'instrukcijeApp.controllers.user.ProfileCtrl',
        'instrukcijeApp.controllers.admin.CreateAdCtrl',
        'instrukcijeApp.controllers.user.DisplayingAdsCtrl',
        'instrukcijeApp.controllers.admin.EditAdCtrl',
        'instrukcijeApp.controllers.user.AdDetailsCtrl',
        'instrukcijeApp.controllers.AboutUsCtrl',
        'instrukcijeApp.controllers.CreateSuperadminCtrl',

        'instrukcijeApp.services.UserService',
        'instrukcijeApp.services.AdminService',
        'instrukcijeApp.services.Http',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngRoute',
        'ngAnimate',
        'ngTouch'
      ])
      .run(['$rootScope', '$location', 'UserService',
        function(
        $rootScope,
        $location,
        UserService
        ) {
        $rootScope.changeView = function(path) {
          $location.path(path);
        };
        
        $rootScope.currentUser = UserService.getCurrentUser;
        
        $rootScope.onLogoutClick = function() {
          $rootScope.changeView('/logout');        
        };
      }])
      .config(function ($routeProvider, $httpProvider) {
        $httpProvider.defaults.withCredentials = true;

        $routeProvider
          .when('/', {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl',
            controllerAs: 'main'
          })
          .when('/dashboard/:userId', {
            templateUrl: 'views/dashboard.html',
            controller: 'DashboardCtrl',
            controllerAs: 'dashboard'
          })
          .when('/register', {
            templateUrl: 'views/user/register.html',
            controller: 'RegisterCtrl',
            controllerAs: 'register'
          })
          .when('/login', {
            templateUrl: 'views/user/login.html',
            controller: 'LoginCtrl',
            controllerAs: 'login'
          })
          .when('/superadmin', {
            templateUrl: 'views/superadmin/superadmin.html',
            controller: 'SuperadminCtrl',
            controllerAs: 'superadmin'
          })
           .when('/superadmin/edit', {
            templateUrl: 'views/superadmin/edit.html',
            controller: 'EditCtrl',
            controllerAs: 'edit'
          })
          .when('/superadmin/add-admin', {
            templateUrl: 'views/superadmin/add-admin.html',
            controller: 'AddAdminCtrl',
            controllerAs: 'addAdmin'
          })
          .when('/admin', {
            templateUrl: 'views/admin/admin.html',
            controller: 'AdminCtrl',
            controllerAs: 'admin'
          })
          .when('/admin/new', {
            templateUrl: 'views/admin/add-user.html',
            controller: 'AddUserCtrl',
            controllerAs: 'addUser'
          })
          .when('/logout', { 
            templateUrl: 'views/logout/logout.html',
            controller: 'LogoutCtrl',
            controllerAs: 'Logout'
          })
          .when('/superadmin/displaying-admin', {
            templateUrl: 'views/superadmin/displaying-admin.html',
            controller: 'DisplayingAdminCtrl',
            controllerAs: 'DisplayingAdmin'
          })
          .when('/user/error', {
            templateUrl: 'views/user/error.html',
            controller: 'ErrorCtrl',
            controllerAs: 'Error'
          })
          .when('/admin/displaying-users', {
            templateUrl: 'views/admin/displaying-users.html',
            controller: 'DisplayingUsersCtrl',
            controllerAs: 'DisplayingUsers'
          })
          .when('/admin/create-user', {
            templateUrl: 'views/admin/create-user.html',
            controller: 'CreateCtrl',
            controllerAs: 'CreateCtrl'
          })
          .when('/user/profile/:userId', {
            templateUrl: 'views/user/profile.html',
            controller: 'ProfileCtrl',
            controllerAs: 'ProfileCtrl'
          })
          .when('/admin/create-ad', {
            templateUrl: 'views/admin/create-ad.html',
            controller: 'CreateAdCtrl',
            controllerAs: 'CreateAdCtrl'
          })
          .when('/user/displaying-ads', {
            templateUrl: 'views/user/displaying-ads.html',
            controller: 'DisplayingAdsCtrl',
            controllerAs: 'DisplayingAds'
          })
          .when('/admin/edit-ad/:adId', {
            templateUrl: 'views/admin/edit-ad.html',
            controller: 'EditAdCtrl',
            controllerAs: 'EditAd'
          })
          .when('/user/ad-details/:adId', {
            templateUrl: 'views/user/ad-details.html',
            controller: 'AdDetailsCtrl',
            controllerAs: 'AdDetails'
          })
           .when('/about-us', {
            templateUrl: 'views/about-us.html',
            controller: 'AboutUsCtrl',
            controllerAs: 'AboutUs'
          })
            .when('/create-superadmin', {
            templateUrl: 'views/create-superadmin.html',
            controller: 'CreateSuperadminCtrl',
            controllerAs: 'CreateSuperadmin'
          })
          .otherwise({
            redirectTo: '/not-found'
          });
        });
  });
