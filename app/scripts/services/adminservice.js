define(['angular'], function (angular) {
  'use strict';

	angular.module('instrukcijeApp.services.AdminService', [])
  .service('AdminService', function () {
  	var admins =  [{
			id: 1,
			name: "Nikola",
			email: "nikola@gmail.com"
		}, {
			id: 2,
			name: "Nikola",
			email: "nikica.nikic@gmail.com"
		}, {
			id: 3,
			name: "Ivan",
			email: "ivan.ivkovic@gmail.com"
		}, {
			id: 1,
		  name: "Miro",
		  email: "miro@gmail.com"
		}, {
			id: 2,
			name: "Hana",
			email: "h.hana@gmail.com"
		}, {
			id: 3,
			name: "Davor",
			email: "h.davor@gmail.com"
		}];

    var getAdmins = function() {
      return admins;
    };

    var setAdmin = function(newAdmin) {
    	newAdmin.id = 2
 			admins.push(newAdmin);
    };

    return {
    	getAdmins: getAdmins,
    	setAdmin: setAdmin
    };
	});
});

6