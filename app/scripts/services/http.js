define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.services.Http', [])
	.service('Http', ['$http', function ($http) {

    // options = {
    //   method,
    //   url,
    //   data
    // }
    var makeApiCall = function(options) {
      var httpPromise = $http({
        method: options.method,
        url: 'http://localhost:3000/' + options.url,
        data: options.data,
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        }
      })
      .catch(function(response) {
        if (response.status === 401) {
          localStorage.setItem('currentUser', null);
          $scope.changeView('/login');
        } else if (response.status === 403) {
          $scope.changeView('/error');
        }
      });

      return httpPromise;
    };

    return {
      makeApiCall: makeApiCall
    };
	}]);
});
