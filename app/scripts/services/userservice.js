define(['angular'], function (angular) {
  'use strict';

  angular.module('instrukcijeApp.services.UserService', [])
	.service('UserService', function () {
    var currentUser = {};
    
    var setCurrentUser = function(user) {
      currentUser = user;

      var currentUserStringified = JSON.stringify(currentUser);
      localStorage.setItem('currentUser', currentUserStringified);
    };

    var getCurrentUser = function() {
      if (!currentUser._id) {
        var currentUserStringified = localStorage.getItem('currentUser');
        var currentUserFromLocalStorage = JSON.parse(currentUserStringified);
        
        if (!currentUserFromLocalStorage) {
          currentUserFromLocalStorage = {};
        }

        setCurrentUser(currentUserFromLocalStorage);
      }

      return currentUser;
    };

    return {
      getCurrentUser: getCurrentUser,
      setCurrentUser: setCurrentUser
    };
	});
});