/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Service: Http', function () {

    // load the service's module
    beforeEach(module('instrukcijeApp.services.Http'));

    // instantiate service
    var Http;
    beforeEach(inject(function (_Http_) {
      Http = _Http_;
    }));

    it('should do something', function () {
      expect(!!Http).toBe(true);
    });

  });
});
