/*jshint unused: vars */
define(['angular', 'angular-mocks', 'app'], function(angular, mocks, app) {
  'use strict';

  describe('Service: UserService', function () {

    // load the service's module
    beforeEach(module('instrukcijeApp.services.UserService'));

    // instantiate service
    var UserService;
    beforeEach(inject(function (_UserService_) {
      UserService = _UserService_;
    }));

    it('should do something', function () {
      expect(!!UserService).toBe(true);
    });

  });
});
